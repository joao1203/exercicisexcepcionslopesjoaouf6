/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/16
* TITLE: Llegir un fitxer
 */

import java.io.File
import java.io.IOException
import java.util.*

private val red = "\u001b[31;1m"
private val reset = "\u001B[0m"

fun main(){
    val sc = Scanner(System.`in`)
    println("Enter the name of the file: (working: names.txt)")
    val fileName = sc.next()
    readFile(fileName)
}

fun readFile(fileName: String){
    val file = File("./src/main/kotlin/$fileName")
    try {
        println(file.readText())
    }
    catch (e: IOException){
        println("There has been a entry/exit error:")
        println("$red$fileName (System cannot find the specified file)$reset")
    }
}
