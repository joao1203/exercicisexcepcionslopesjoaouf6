/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/16
* TITLE: Pasar a Double o 1.0
 */

import java.util.*

fun main(){
    val sc = Scanner(System.`in`)
    println("Enter the number:")
    val turnToDouble = sc.next()
    println(toDoubleOrOne(turnToDouble))
}

fun toDoubleOrOne(turned: String): Double{
    var result = 1.0
    try {
        result = turned.toDouble()
    }
    catch (e: NumberFormatException){
        result = 1.0
    }
    return result
}