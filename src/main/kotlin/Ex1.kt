/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/16
* TITLE: Divideix o Cero
 */

import java.util.*

fun main(){
    val sc = Scanner(System.`in`)
    println("Enter the dividend:")
    val dividend = sc.nextInt()
    println("Enter the divisor:")
    val divisor = sc.nextInt()
    println(divideOrZero(dividend,divisor))
}

fun divideOrZero(dividend: Int, divisor: Int): Int{
    var result = 0
    try {
        result = dividend / divisor
    }
    catch (e: ArithmeticException){
        println("ERROR: 0 is not an acceptable divisor!")
    }
    return result
}